import { createI18n } from 'vue-i18n'

export default defineNuxtPlugin(({ vueApp }): void => {
  vueApp.use(createI18n({
    legacy: false,
    globalInjection: true,
    locale: 'en',
    messages: {
      en: {
        'sync-tabs-mode': {
          title: 'Sync Tabs Mode'
        },
        'login-page': {
          inputs: {
            email: 'Email',
            password: 'Password'
          },
          buttons: {
            login: 'Login'
          }
        },
        'main-page': {
          title: 'Notes',
          'empty-notes': 'No notes yet',
          buttons: {
            'add-random-note': 'Add Random Note'
          }
        },
        notes: {
          'remove-note': 'Remove Note'
        }
      },
      ru: {
        'sync-tabs-mode': {
          title: 'Режим синхронизации вкладок'
        },
        'login-page': {
          inputs: {
            email: 'Почта',
            password: 'Пароль'
          },
          buttons: {
            login: 'Войти'
          }
        },
        'main-page': {
          title: 'Заметки',
          'empty-notes': 'Нет заметок',
          buttons: {
            'add-random-note': 'Добавить случайную заметку'
          }
        },
        notes: {
          'remove-note': 'Удалить заметку'
        }
      }
    }
  }))
})
