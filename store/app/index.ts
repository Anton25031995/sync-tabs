import { defineStore } from 'pinia'
import { state } from './state'

export const useAppStore = defineStore('app', {
  state: () => state
})
