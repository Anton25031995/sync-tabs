import { setLanguageFromStorage } from '~/utils/set-language-from-storage'
import { Broadcast } from '~/utils/broadcast'
import { syncTabs } from '~/utils/sync-tabs'
import { sendMessageToOtherTabs } from '~/utils/send-message-to-other-tabs'

export {
  setLanguageFromStorage,
  Broadcast,
  syncTabs,
  sendMessageToOtherTabs
}
