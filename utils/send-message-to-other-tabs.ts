import { useAppStore } from '~/store/app'
import { Broadcast } from '~/utils/broadcast'

export const sendMessageToOtherTabs = (message: any) => {
  const appStore = useAppStore()

  if (appStore.syncTabsMode === 'broadcast') {
    Broadcast.postMessage(message)
    console.log('Message send by Broadcast Channel', message)
  } else if (appStore.syncTabsMode === 'storage') {
    localStorage.setItem('sync-tabs_message', JSON.stringify(message))
    console.log('Message send by Local Storage', message)
  } else if (appStore.syncTabsMode === 'sw') {
    // @ts-ignore
    navigator.serviceWorker.controller.postMessage(message)
    console.log('Message send by Service Worker', message)
  }
}
