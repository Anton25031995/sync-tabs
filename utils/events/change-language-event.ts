import type { Composer } from 'vue-i18n'

export const changeLanguageEvent = (newLanguage: string, { i18n }: { i18n: Composer }) => {
  i18n.locale.value = newLanguage
}
