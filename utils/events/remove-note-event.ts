import { useAppStore } from '~/store/app'

export const removeNoteEvent = (removingNoteHash: number) => {
  const appStore = useAppStore()
  const removingNoteIndex = appStore.notes.findIndex((note: any) => {
    return note.hash === removingNoteHash
  })

  if (removingNoteIndex === -1) {
    return
  }

  appStore.notes.splice(removingNoteIndex, 1)
}
