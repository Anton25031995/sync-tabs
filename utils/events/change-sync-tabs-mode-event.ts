import { useAppStore } from '~/store/app'

export const changeSyncTabsModeEvent = (newMode: string) => {
  const appStore = useAppStore()

  appStore.syncTabsMode = newMode
}
