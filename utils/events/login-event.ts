import type { Router } from 'vue-router'

export const loginEvent = (_: any, { router }: { router: Router }) => {
  router.push({ name: 'index' })
}
