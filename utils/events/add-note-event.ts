import { useAppStore } from '~/store/app'

export const addNoteEvent = (noteData: any) => {
  const appStore = useAppStore()

  // @ts-ignore
  appStore.notes.push(noteData)
}
