import { changeLanguageEvent } from '~/utils/events/change-language-event'
import { loginEvent } from '~/utils/events/login-event'
import { addNoteEvent } from '~/utils/events/add-note-event'
import { removeNoteEvent } from '~/utils/events/remove-note-event'
import { changeSyncTabsModeEvent } from '~/utils/events/change-sync-tabs-mode-event'

export {
  changeLanguageEvent,
  loginEvent,
  addNoteEvent,
  removeNoteEvent,
  changeSyncTabsModeEvent
}
