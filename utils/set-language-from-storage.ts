import { useI18n } from 'vue-i18n'

export const setLanguageFromStorage = () => {
  const language = localStorage.getItem('language')

  if (!language) {
    return
  }

  const i18n = useI18n()

  i18n.locale.value = language
}
