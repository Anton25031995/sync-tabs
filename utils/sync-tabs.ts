import { useI18n } from 'vue-i18n'
import { useRouter } from 'vue-router'
import { useAppStore } from '~/store/app'
import { Broadcast } from '~/utils/broadcast'
import {
  changeLanguageEvent,
  loginEvent,
  addNoteEvent,
  removeNoteEvent,
  changeSyncTabsModeEvent
} from '~/utils/events'

const EVENTS_BY_NAMES = {
  'change-language': changeLanguageEvent,
  'login': loginEvent,
  'add-note': addNoteEvent,
  'remove-note': removeNoteEvent,
  'change-sync-tabs-mode': changeSyncTabsModeEvent
}

export const syncTabs = () => {
  const i18n = useI18n()
  const router = useRouter()
  const appStore = useAppStore()
  const ctx = { i18n, router }

  Broadcast.onmessage = ((event) => {
    if (appStore.syncTabsMode !== 'broadcast') {
      return
    }

    const { name, value } = event.data

    // @ts-ignore
    EVENTS_BY_NAMES[name](value, ctx)

    console.log('Message received by Broadcast Channel', event)
  })

  window.addEventListener('storage', (event) => {
    if (appStore.syncTabsMode !== 'storage') {
      return
    }

    if (event.storageArea != localStorage || event.key !== 'sync-tabs_message') {
      return
    }

    const { name, value } = JSON.parse(event.newValue!)

    // @ts-ignore
    EVENTS_BY_NAMES[name](value, ctx)

    console.log('Message received by Local Storage', event)
  })

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js')
      .then(() => {
        navigator.serviceWorker.addEventListener('message', (event) => {
          if (appStore.syncTabsMode !== 'sw') {
            return
          }

          const { name, value } = event.data

          // @ts-ignore
          EVENTS_BY_NAMES[name](value, ctx)

          console.log('Message received by Service Worker', event)
        })
      })
      .catch((error) => {
        console.error('registration error : ', error)
      })
  }
}
