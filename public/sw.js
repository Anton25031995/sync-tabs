self.addEventListener('message', async (event) => {
  const allClients = await clients.matchAll()

  for (const client of allClients) {
    if (!client.focused) {
      client.postMessage(event.data)
    }
  }
})
